---
title: "关闭/配置微软 E5 开发人员订阅中的二次验证"
description: "关闭/配置微软 E5 开发人员订阅中的二次验证"
slug: "Multifactor"
date: 2022-07-04T14:45:20+08:00
draft: false
---

二次验证能让账号更加安全，推荐至少将管理员账号设置二次验证。

但是二次验证也会阻止第三方应用的访问。所以其他子账号可以直接关闭。


## 使用二次验证

为了保证账号的安全，微软对于账号默认有二次验证的需求，推荐使用谷歌验证器进行验证（界面比较美观）。

## 关闭二次验证

登录 [所有服务 - Azure Active Directory 管理中心](https://aad.portal.azure.com/#allservices/category/All)，选择租户属性：

![image-20220704144948794](image-20220704144948794.png)

打开界面拉到最后点击管理安全默认值

![image-20220704145130116](image-20220704145130116.png)

在这个启用安全值界面后点击否即可，下次登录就不会在收到开启Authenticator验证码了，也能够支持第三方登录。

![image-20220704145210493](image-20220704145210493.png)