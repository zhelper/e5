## 说明

此部分仅供参考，E5续期一直是一个玄学问题，不过正常进行“开发活动”，续期的概率也是非常大的。

此部分划为三个板块，第一板块介绍一款综合性的Plus软件，功能较为强大，推荐有能力的朋友折腾。

第二板块介绍调用Outlook进行续期的工具。如果你有TG，可以直接使用TG E5 BOt。

第三板块简单介绍调用OneDrive进行续期。如果您有列表程序/文件分享的需求，可以在搭建的同时，顺便进行调用续期。

## Microsoft 365 E5 Renew Plus

某位大佬写的综合性的调用工具，在一个软件里面就可以调用一大堆资源。

说明：由于我开启了二次验证，故最后配置的是非登录调用的API，这是对官方教程的缩减以及补充。以及，推荐弄一台Windows服务器跑，以保证每天调用，增加活跃度。

[官方文档](https://ednovas.xyz/2022/01/10/e5renewplus/)

[官方频道](https://t.me/MS365E5Renew)

[下载链接](https://sundayrx.lanzoui.com/vqTrXlR5LrWjP3F)

下载解压后，安装必要组件，打开软件，点击中间的**开始运行**。

由于本软件涉及到各种API的调用，所以我们先要到微软的开发人员中心配置好权限等才能使用。

### 新建应用程序

点击直接进入[Azure应用注册](https://portal.azure.com/#blade/Microsoft_AAD_RegisteredApps/ApplicationsListBlade)，登录账号使用申请到的 Microsoft 365 E5 的管理员账户（账户名类似 XXXX@YYYY.onmicrosoft.com 格式）。

单击 “新注册” 按钮

![](2022-02-09-18-38-40.png)

配置应用 应用名称随意写、配置可访问性、重定向 URL 暂时不填 、完成后点击注册。会跳转到管理界面。

![](2022-02-09-18-38-58.png)

先点击 “概述”，记下应用程序（客户端）ID，然后点击“添加重定向 URL”，进入重定向 URL 配置界面。

![](2022-02-09-18-39-42.png)

点击 “添加平台”，再点击 “移动和桌面应用程序”：

![](2022-02-09-18-40-56.png)

继续勾选中第一个 URL，最后点击底部的 “配置”：

![](2022-02-09-18-41-11.png)

配置默认客户端类型将应用程序视为公共客户端，点击切换按钮为“是”，**最后点击“保存”按钮保存。**

### 配置权限

![](2022-02-09-18-41-25.png)

点击 “API 权限”-“添加权限”-“Microsoft Graph”

![](2022-02-09-18-42-25.png)

选择 “应用程序权限”

![](2022-02-09-18-44-43.png)

根据下方列出的 API 权限需求表来勾选所对应的 API 权限，全部选择完成后点击” 添加权限”。（善用搜索功能）
Calendars.Read; Contacts.Read; Directory.Read.All; Files.Read.All; Files.ReadWrite.All; Mail.Read;
Mail.Send; MailboxSettings.Read; Notes.Read.All; Sites.Read.All; User.Read.All;

![](2022-02-09-18-43-15.png)

添加完成后，点击代表 XXX 授予管理员同意，对话框选择 “是”。如果没有 “代表 XXX 授予管理员同意” 按钮 说明该账号不是管理员账号 换登管理员账号创建应用。

![](2022-02-09-18-45-22.png)

### 新建机密

点击 “证书密码”-“新客户端密码”-“24 个月”-“添加”：

![](2022-02-09-18-45-40.png)

点击 “值” 该列中的 “复制”（不要复制”ID” 列中的值），并立即将该密码保存至电脑，保存的值即为 “客户端密码”， 注：该值必须立即保存，退出该页面后将永远无法查看。

![](2022-02-09-18-46-45.png)

## 配置软件

点击 “开始运行”-“添加账号”

![](2022-02-09-18-47-28.png)

如图，粘贴刚才复制的ID和机密：

![](2022-02-09-18-48-07.png)

点击确定，开始运行即可。

## 调用Outlook API

常见的方式之一是调用OutlookAPI

## E5SubBot

一款调用outlookAPI进行续期的TGBOT。使用此工具会将secret传给客户端进行储存，可能会有信息泄露的风险，请注意。

机器人地址：https://t.me/E5Sub_bot

项目地址：https://github.com/iyear/E5SubBot

使用方法：

```
在机器人对话框输入 /bind
注册应用，使用E5主账号或同域账号登录，跳转页面获得client_secret。点击回到快速启动,获得client_id
复制client_secret和client_id，以 client_id client_secret格式回复
获得授权链接，使用E5主账号或同域账号登录
授权后会跳转至http://localhost/e5sub…… (会提示网页错误，复制链接即可)
复制整个浏览框内容，在机器人对话框回复 链接+空格+别名(用于管理账户) 例如：http://localhost/e5sub/?code=abcd MyE5，等待机器人绑定后即完成
```

## Github Action outlook api

也可以自己使用GitHub Action手动配置：

[利用Github Actions刷Microsoft 365 E5开发者订阅API实现续订-如有乐享](https://51.ruyo.net/15646.html)

备注：这篇文章年代有点久了，不保证可用性。

## 调用Onedrive API

一般而言，只要是涉及到第三方对OneDrive内容读写的，都有几率续期。特别的，如果使用自定义注册应用程序，续期几率应该是更高。此部分可以参考 使用E5订阅中的onedrive网盘 部分。

参考：

1. https://ednovas.xyz/2022/01/10/e5renewplus/