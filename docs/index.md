## 微软开发人员E5订阅完全食用指南

【摘要】简单介绍了微软开发人员E5订阅的注册、续期和使用。包括OneDrive大容量网盘、office专业版、exchang、e企业邮箱。以及用户管理的相关内容。

【关键词】免费，E5，微软，office，专业版，OneDrive，5T，25T，exchange，企业邮箱，转发规则，收发信息。

本部分是 Zhelper 项目的一部分。

如果您希望参与到本文档的编辑中，或是对教程有不理解的地方，可以加入 [TG群](https://t.me/zhelper)（国内无法直接访问） 或 [论坛](https://bbs.zhelper.net/)
