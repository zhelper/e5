## 获取微软账户

首先，您需要有一个微软账户。如果没有，可以转到[本网站](https://signup.live.com)进行注册。

## 注册开发人员账号

转到：[开发人员计划 - Microsoft 365](https://developer.microsoft.com/zh-CN/microsoft-365/dev-program#Subscription)

选择`立即加入`。然后按以下操作即可。

![仅作示范](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20211213172519564.png)

![仅作示范](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20211213172536585.png)

![仅作示范](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20211213172524878.png)

## 注册E5沙盒

一般而言，完成以上注册之后会自动弹出创建沙盒界面，如果没有自动弹出，也可以手动点击`创建沙盒`或者`sandbox`进行创建。

![注册沙盒](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20211213172618589.png)

![管理员信息，注意地区不可更改](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20211213172921577.png)

![添加手机号](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20211213173002359.png)

## 验证问题

进行手机号验证时，有概率提示验证失败（发生错误），这可能是微软服务器暂时的问题，但更大概率是注册的环境有问题。这时，不推荐在原有环境下重复注册。

您可以尝试：切成英文界面[链接](https://developer.microsoft.com/en-us/microsoft-365/profile?source=transaction-welcome-email)、换成国外网络（使用代理）后重试。

如果您还是无法解决问题，可以发送邮件寻求帮助。

一种常见的失败提示：

![注册失败](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20211213174016252.png)

注册成功的显示：

![订阅信息](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20211219232724433.png)

## 简单的使用

注册成功之后，转到[Office](https://www.office.com/)，利用刚才注册的开发人员账号（@XXX.onmicrosoft.com）登录即可。